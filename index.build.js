var fs = require('fs'),
async = require('async');
const raml2html = require('raml2html');
const configWithDefaultTheme = raml2html.getConfigForTheme();
const open = require('open');
var dirPath = 'docs/'; //provice here your path to dir
var ramlContent = '';
fs.readFile('base.raml','utf8',(err,content)=>{
  if(err) throw err;
  ramlContent = content;
  readDocs(ramlContent);
});

const readDocs = (ramlContent) => {

  fs.readdir(dirPath, function (err, filesPath) {
    if (err) throw err;
    filesPath = filesPath.map(function(filePath){ //generating paths to file
      return dirPath + filePath;
    });
    async.map(filesPath, function(filePath, cb){ //reading files or dir
      console.log('reading '+ filePath +' ...');
      fs.readFile(filePath, 'utf8', cb);
    }, function(err, results) {
      for(var i = 0; i < results.length;i++){
        ramlContent+= results[i];
      }
      fs.writeFile('digikala.raml',ramlContent,function(){

        // source can either be a filename, url, or parsed RAML object
        raml2html.render('digikala.raml', configWithDefaultTheme).then(function(result) {
          fs.writeFile('index.html',result,()=>console.log('Raml file generated in digikala.raml'));
          // Save the result to a file or do something else with the result
        }, function(error) {
          // Output error
        });
      });

    });
  });
}
