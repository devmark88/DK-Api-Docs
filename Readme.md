# What is this?

This documentation is base on [RAML](https://raml.org/).

This application read all files in "docs" folder and gather their content into a file named "digikala.raml" and finally generate HTML from it.

TIP: The base.raml file is based of raml!!! all the content gathered from docs folder append in the end of this file. In this file I defined [Raml data types](https://github.com/raml-org/raml-spec/blob/master/versions/raml-10/raml-10.md#raml-data-types).

You can run this application in PHP or node js.

# How to run in Node
The node version is based on [Raml2Html](https://github.com/raml2html/raml2html).

### Prerequirement:

You only need node and npm to start project.

### How to run:
clone the repository and install node modules.
```
npm install
```
Then you can run project.
```
npm start
```
That's it.

You can change the view template based on [Raml2Html](https://github.com/raml2html/raml2html) documentaion.

# How to run in PHP
The php version is based on [PHP Raml2Html](https://github.com/mikestowe/php-raml2html)

### Prerequirement:
You need enable curl php extension and simply run php command like this

```
php -S localhost:6030
```

===========================================

TIPS:

You can validate generated Raml file in [Restlet Studio](https://studio.restlet.com/).

You can use another libraries to create view from raml file. The bottom line of the documentation is the RAML file and how it should be generate.
