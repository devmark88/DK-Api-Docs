const express = require("express");
const path = require('path');
var fs = require('fs');
async = require('async');
const raml2html = require('raml2html');
const configWithDefaultTheme = raml2html.getConfigForTheme();
const open = require('open');
var dirPath = 'docs/'; //provice here your path to dir
var ramlContent = '';
console.log('read base.raml...');
fs.readFile('base.raml','utf8',(err,content)=>{
  if(err) throw err;
  ramlContent = content;
  console.log('start surffing documentations...');
  readDocs(ramlContent);

});

const readDocs = (ramlContent) => {

  fs.readdir(dirPath, function (err, filesPath) {
    if (err) throw err;
    filesPath = filesPath.map(function(filePath){ //generating paths to file
      return dirPath + filePath;
    });
    async.map(filesPath, function(filePath, cb){ //reading files or dir
      console.log('reading '+ filePath +' ...');
      fs.readFile(filePath, 'utf8', cb);
    }, function(err, results) {
      for(var i = 0; i < results.length;i++){
        ramlContent+= results[i];
      }
      console.log('writing on digikala.raml');
      fs.writeFile('digikala.raml',ramlContent,function(){

        console.log('rendering HTML from raml file...');
        // source can either be a filename, url, or parsed RAML object
        raml2html.render('digikala.raml', configWithDefaultTheme).then(function(result) {
          console.log('wrinting in index.html...');
          fs.writeFile('index.html',result,()=>{
            const port = 6031;
            const app = express();


            app.listen(port, (error) => {
                if (error) {
                    console.error(error);
                } else {
                    open(`http://localhost:${port}`)
                }
            });
            app.get('*',  (req, res) => {
                res.sendFile(path.join(__dirname, '/index.html'));
            });

          });
          console.log('*** DONE... *** your html and raml file are ready to use');
          // Save the result to a file or do something else with the result
        }, function(error) {
          // Output error
        });
      });

    });
  });
}
